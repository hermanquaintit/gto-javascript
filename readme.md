Задачи Цифровое ГТО от ОБУ "ИТЦ"<br>
======

БЛОК 1<br>
------

<h3>Задача 1</h3>

*По данному целому числу, найдите его квадрат.*<br>

**Входные данные:**<br>
На вход дается одно целое число, по модулю не превосходящее 100.<br>
**Пример ввода:**<br>
5<br>

**Выходные данные:**<br>
Программа должна вывести квадрат данного числа.<br>
**Пример вывода:**<br>
25<br>

---

<h3>Задача 2</h3>

_Пирожок в столовой стоит x рублей и y копеек. Определите, сколько рублей и копеек
нужно заплатить за m пирожков._<br>

**Входные данные:**<br>
Программа получает на вход три целых числа: x, y, m.<br>
**Пример ввода:**<br>
5<br>
50<br>
2<br>

**Выходные данные:**<br>
Программа должна вывести два числа: стоимость покупки в рублях и копейках.<br>
**Пример вывода:**<br>
11<br>
0<br>

---

<h3>Задача 3</h3>

_Дано одно натуральное четырехзначное число. Найдите самое большое число,
полученное из заданного перестановкой его цифр.<br>
Например, если дано число 5239. Из всех перестановок цифр наибольшим будет 9532._<br>

**Входные данные:**<br>
На вход дается одно натуральное четырехзначное число._<br>
**Пример ввода:**
1. 5239<br>
2. 5329<br>

**Выходные данные:**<br>
Выведите одно натуральное число - самое большое число, полученное из заданного
перестановкой цифр заданного числа.<br>
**Пример вывода:**
1. 9532<br>
2. 9532<br>

---

<h3>Задача 4</h3>

_Сегодня Вася ждёт в гости своего друга Вадима. Чтобы подготовиться к встрече, Васе
необходимо посетить два магазина, расположенных рядом с его домом. От дома до
первого магазина ведёт дорожка длины a1 метров, а до второго магазина ведёт
дорожка длины a2 метров. Также существует дорожка, непосредственно соединяющая
два магазина друг с другом, длиной a3 метров.
Помогите Васе вычислить минимальное расстояние, которое ему потребуется
пройти, чтобы посетить оба магазина и вернуться домой.
Вася всегда стартует дома. Он должен посетить оба магазина, перемещаясь только
по имеющимся трём дорожкам, и вернуться назад домой. При этом его совершенно не
смутит, если ему придётся посетить один и тот же магазин или пройти по одной и
той же дорожке более одного раза. Единственная его задача — минимизировать
суммарное пройденное расстояние._<br>

**Входные данные:**<br>
Даны три целых числа **a1, a2, a3** :<br>
**a1** — длина дорожки, соединяющей дом Васи и первый магазин;<br>
**a2** — длина дорожки, соединяющей дом Васи и второй магазин;<br>
**a3** — длина дорожки, соединяющей два магазина.<br>
**Пример ввода:**<br>
1. 10<br>
   20<br>
   30<br>
   <br>
2. 1<br>
   1<br>
   5<br>
   
**Выходные данные:**<br>
Выведите минимальное количество метров, которое придётся пройти Артуру, чтобы
посетить оба магазина и вернуться домой.<br>
**Пример вывода:**<br>
   
1. 60<br>
2. 4<br>

_Примечание:<br>
Одним из оптимальных маршрутов является: дом -> первый магазин -> второй 
магазин -> дом.<br>
Во втором примере одним из оптимальных маршрутов является: дом -> первый 
магазин -> дом -> второй магазин -> дом._
   
БЛОК 2<br>
------

<h3>Задача 1</h3>

_Автоморфное число — неотрицательное целое число, десятичная запись квадрата
которого оканчивается цифрами самого этого числа.<br>
Например, число 25, так как 252 = 625. Также число 625, так как 6252 = 390625.<br>
По данному числу k выведите все автоморфные числа, которые не превосходят k._<br>

**Входные данные:**
Программа получает на вход одно натуральное число k, не превосходящее 10<sup>6</sup>.<br>
**Пример ввода:**<br>
1. 25<br>
2. 10<br>
   
**Выходные данные:**
Программа должна вывести все автоморфные числа в порядке возрастания на одной
строке через пробел, каждое из которых не превосходит k.<br>
**Пример вывода:**<br>
1. 0 1 5 6 25<br>
2. 0 1 5 6<br>

---

<h3>Задача 2</h3>

_Дан список чисел. Определите, есть ли в нем два противоположных (то есть дающих в
сумме 0) числа. Если такие числа есть в массиве, выведите их индексы в порядке
возрастания. Если таких чисел в массиве нет, ничего не выводите._<br>

**Входные данные:**<br>
Сначала задано число N — количество элементов в массиве от 1 до 100 включительно.
Далее через пробел записаны N чисел — элементы массива. Массив состоит из целых
чисел, каждое из которых по модулю не превосходит 100.<br>
**Пример ввода:**<br>
7<br>
1 2 3 4 5 6 -3<br>

**Выходные данные:**<br>
Определите, есть ли в списке два противоположных числа. Если такие числа есть,
выведите их индексы в порядке возрастания через пробел. Если таких чисел в массиве
нет, ничего не выводите.<br>

Примечание: гарантируется, что таких пар не больше одной. Также гарантируется, что в
массиве не более одного нуля.<br>

**Пример вывода:**<br>
2 6<br>

---

<h3>Задача 3</h3>

_Проверьте, является ли число простым._<br>

**Входные данные:**<br>
Вводится одно натуральное число n, принимающие значения от 2 до 2*105.<br>
**Пример ввода:**<br>
1. 3<br>
2. 4<br>

**Выходные данные:**<br>
Необходимо вывести "prime", если число простое, или "composite", если число составное.<br>
**Пример вывода:**<br>
1. prime<br>
2. composite<br>

_Примечание: данную задачу предполагается решить с помощью функций._<br>

---

<h3>Задача 4</h3>

_Кажется, еще совсем недавно наступил новый 2013 год. А знали ли Вы забавный факт о
том, что 2013 год является первым годом после далекого 1987 года, в котором все
цифры различны?<br>
Теперь же Вам предлагается решить следующую задачу: задан номер года, найдите
наименьший номер года, который строго больше заданного и в котором все цифры
различны._<br>
**Входные данные:**<br>
В единственной строке задано целое число y (1000 ≤ y ≤ 9000) — номер года.
Пример ввода:<br>
1) 1987<br>

**Выходные данные:**<br>
Выведите единственное целое число — минимальный номер года, который строго
больше y, в котором все цифры различны. Гарантируется, что ответ существует.<br>
**Пример вывода:**<br>
1) 2013<br>
Задачи Цифровое ГТО от ОБУ "ИТЦ"<br>
======

БЛОК 1<br>
------

<h3>Задача 1</h3>

*По данному целому числу, найдите его квадрат.*<br>

**Входные данные:**<br>
На вход дается одно целое число, по модулю не превосходящее 100.<br>
**Пример ввода:**<br>
5<br>

**Выходные данные:**<br>
Программа должна вывести квадрат данного числа.<br>
**Пример вывода:**<br>
25<br>

---

<h3>Задача 2</h3>

_Пирожок в столовой стоит x рублей и y копеек. Определите, сколько рублей и копеек
нужно заплатить за m пирожков._<br>

**Входные данные:**<br>
Программа получает на вход три целых числа: x, y, m.<br>
**Пример ввода:**<br>
5<br>
50<br>
2<br>

**Выходные данные:**<br>
Программа должна вывести два числа: стоимость покупки в рублях и копейках.<br>
**Пример вывода:**<br>
11<br>
0<br>

---

<h3>Задача 3</h3>

_Дано одно натуральное четырехзначное число. Найдите самое большое число,
полученное из заданного перестановкой его цифр.<br>
Например, если дано число 5239. Из всех перестановок цифр наибольшим будет 9532._<br>

**Входные данные:**<br>
На вход дается одно натуральное четырехзначное число._<br>
**Пример ввода:**
1. 5239<br>
2. 5329<br>

**Выходные данные:**<br>
Выведите одно натуральное число - самое большое число, полученное из заданного
перестановкой цифр заданного числа.<br>
**Пример вывода:**
1. 9532<br>
2. 9532<br>

---

<h3>Задача 4</h3>

_Сегодня Вася ждёт в гости своего друга Вадима. Чтобы подготовиться к встрече, Васе
необходимо посетить два магазина, расположенных рядом с его домом. От дома до
первого магазина ведёт дорожка длины a1 метров, а до второго магазина ведёт
дорожка длины a2 метров. Также существует дорожка, непосредственно соединяющая
два магазина друг с другом, длиной a3 метров.
Помогите Васе вычислить минимальное расстояние, которое ему потребуется
пройти, чтобы посетить оба магазина и вернуться домой.
Вася всегда стартует дома. Он должен посетить оба магазина, перемещаясь только
по имеющимся трём дорожкам, и вернуться назад домой. При этом его совершенно не
смутит, если ему придётся посетить один и тот же магазин или пройти по одной и
той же дорожке более одного раза. Единственная его задача — минимизировать
суммарное пройденное расстояние._<br>

**Входные данные:**<br>
Даны три целых числа **a1, a2, a3** :<br>
**a1** — длина дорожки, соединяющей дом Васи и первый магазин;<br>
**a2** — длина дорожки, соединяющей дом Васи и второй магазин;<br>
**a3** — длина дорожки, соединяющей два магазина.<br>
**Пример ввода:**<br>
1. 10<br>
   20<br>
   30<br>
   <br>
2. 1<br>
   1<br>
   5<br>
   
**Выходные данные:**<br>
Выведите минимальное количество метров, которое придётся пройти Артуру, чтобы
посетить оба магазина и вернуться домой.<br>
**Пример вывода:**<br>
   
1. 60<br>
2. 4<br>

_Примечание:<br>
Одним из оптимальных маршрутов является: дом -> первый магазин -> второй 
магазин -> дом.<br>
Во втором примере одним из оптимальных маршрутов является: дом -> первый 
магазин -> дом -> второй магазин -> дом._
   
БЛОК 2<br>
------

<h3>Задача 1</h3>

_Автоморфное число — неотрицательное целое число, десятичная запись квадрата
которого оканчивается цифрами самого этого числа.<br>
Например, число 25, так как 252 = 625. Также число 625, так как 6252 = 390625.<br>
По данному числу k выведите все автоморфные числа, которые не превосходят k._<br>

**Входные данные:**
Программа получает на вход одно натуральное число k, не превосходящее 106 .<br>
**Пример ввода:**<br>
1. 25<br>
2. 10<br>
   
**Выходные данные:**
Программа должна вывести все автоморфные числа в порядке возрастания на одной
строке через пробел, каждое из которых не превосходит k.<br>
**Пример вывода:**<br>
1. 0 1 5 6 25<br>
2. 0 1 5 6<br>

---

<h3>Задача 2</h3>

_Дан список чисел. Определите, есть ли в нем два противоположных (то есть дающих в
сумме 0) числа. Если такие числа есть в массиве, выведите их индексы в порядке
возрастания. Если таких чисел в массиве нет, ничего не выводите._<br>

**Входные данные:**<br>
Сначала задано число N — количество элементов в массиве от 1 до 100 включительно.
Далее через пробел записаны N чисел — элементы массива. Массив состоит из целых
чисел, каждое из которых по модулю не превосходит 100.<br>
**Пример ввода:**<br>
7<br>
1 2 3 4 5 6 -3<br>

**Выходные данные:**<br>
Определите, есть ли в списке два противоположных числа. Если такие числа есть,
выведите их индексы в порядке возрастания через пробел. Если таких чисел в массиве
нет, ничего не выводите.<br>

Примечание: гарантируется, что таких пар не больше одной. Также гарантируется, что в
массиве не более одного нуля.<br>

**Пример вывода:**<br>
2 6<br>

---

<h3>Задача 3</h3>

_Проверьте, является ли число простым._<br>

**Входные данные:**<br>
Вводится одно натуральное число n, принимающие значения от 2 до 2*105.<br>
**Пример ввода:**<br>
1. 3<br>
2. 4<br>

**Выходные данные:**<br>
Необходимо вывести "prime", если число простое, или "composite", если число составное.<br>
**Пример вывода:**<br>
1. prime<br>
2. composite<br>

_Примечание: данную задачу предполагается решить с помощью функций._<br>

---

<h3>Задача 4</h3>

_Кажется, еще совсем недавно наступил новый 2013 год. А знали ли Вы забавный факт о
том, что 2013 год является первым годом после далекого 1987 года, в котором все
цифры различны?<br>
Теперь же Вам предлагается решить следующую задачу: задан номер года, найдите
наименьший номер года, который строго больше заданного и в котором все цифры
различны._<br>
**Входные данные:**<br>
В единственной строке задано целое число y (1000≤y≤9000) — номер года.
Пример ввода:<br>
1) 1987<br>

**Выходные данные:**<br>
Выведите единственное целое число — минимальный номер года, который строго
больше y, в котором все цифры различны. Гарантируется, что ответ существует.<br>
**Пример вывода:**<br>
1) 2013<br>
