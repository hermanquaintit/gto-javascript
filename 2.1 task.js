/*Блок 2 Задача 1*/

let k = Number(prompt('Введите натуральное число не превосходящее 10^6', "25"));

//let k = 25;

let mod = 10;
let arr = [];

for (let i = 0; i <= k; i++) {
    if (i === mod) mod = mod * 10;
    if (i * i % mod === i) arr.push(i);
}
console.log(arr.join(' '));
