/*Блок 2 Задача 3*/

let n = Number(prompt('Введите натуральное число от 2 до 2*10^5', "3"));

//let n = 3;

function isPrime(n) {
    if (n % 2 === 0) {
        return n === 2;
    }
    let d = 3
    while (d * d <= n && n % d !== 0) {
        d += 2;
    }
    return d * d > n;
}

console.log(isPrime(n) ? 'prime' : 'composite');